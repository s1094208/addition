import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-addition-form',
  templateUrl: './addition-form.component.html',
  styleUrls: ['./addition-form.component.css']
})
export class AdditionFormComponent implements OnInit {

  additionForm;

  constructor( private formBuilder: FormBuilder) {
    this.additionForm = this.formBuilder.group({
      result: ''
    });
  }

  numberA = 0;
  numberB = 0;
  total = 0;
  evaluation;

  createRandomAddition(): void {
    this.numberA = Math.floor(Math.random() * 100) + 1;
    this.numberB = Math.floor(Math.random() * 100) + 1;
    this.total = this.numberA + this.numberB;
  }

  validateResult(result): void {
    this.evaluation = '';
    if (Number(result) === this.total) {
      this.evaluation = 'You are correct!';
    } else {
      this.evaluation = 'You are wrong! Try again!';
    }
  }

  ngOnInit(): void {
    this.createRandomAddition();
  }

}
